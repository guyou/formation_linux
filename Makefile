all: initiation.html perfectionnement.html avance.html themes.html programmation.html

%.html: %.md
	pandoc --toc -f markdown -t html -s -o $@ $<

%.pdf: %.md
	pandoc --toc -f markdown -o $@ $<
