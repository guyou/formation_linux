% Formation Linux : avancé
% Guilhem Bonnefille <guilhem.bonnefille@gmail.com>
%

Intro
===
Arborescence standard

Programmation

Shell
=====
Cf. http://korben.info/explain-shell.html http://explainshell.com/

SSH
===
Première connexion : vérification et acceptation

Déport d'affichage X

Transfert de fichiers

Authentification par clés

* Génération d'une père de clés
* Phrase de passe
* Clé privée/publique

Agent SSH

Tunneling

Connexion par rebond

Mail
===
Les concepts (SMTP, IMAP, POP)

Impression
===
Partage d'imprimante en réseau.

Scanner
===
Partage de scanner en réseau.

Cf. saned

Son
===
Partage et distribution du son sur le réseau.

Cf. PulseAudio

Références :

* http://tuxicoman.jesuislibre.net/2014/02/pulseaudio-partage-sur-le-lan.html

XBMC
===

Virtualisation
===

Les concepts
---
* Rappels des concepts d'architecture
* Concepts de virtualisation
* Différentes solutions de virtualisation

Cas d'utilisation
---
* plusieurs OS
* essayer avant d'installer
* snapshoting

Mise en pratique
---
* Installation d'Ubuntu (LTS et dernière version)
* Création d'un serveur de partage d'impression, de scan et de diffusion de son
  * à base de RaspPi ou équivalent
  * partage Linux et Windows (samba)
