```
sudo add-apt-repository ppa:yannubuntu/boot-repair
sudo sed 's/trusty/saucy/g' -i /etc/apt/sources.list.d/yannubuntu-boot-repair-trusty.list
sudo apt-get update
sudo apt-get install -y boot-repair && (boot-repair &)
```

http://paste.ubuntu.com/8481547/

```
$ sudo parted /dev/sda unit s print
Error: Can't have overlapping partitions.
```

```
sudo fdisk -l -u /dev/sda

Disk /dev/sda: 500.1 GB, 500107862016 bytes
255 heads, 63 sectors/track, 60801 cylinders, total 976773168 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk identifier: 0x8694284e

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      718847      358400    7  HPFS/NTFS/exFAT
/dev/sda2          718848   666324991   332803072    7  HPFS/NTFS/exFAT
/dev/sda3       666324992   768724992    51200000+   5  Extended
/dev/sda4       768724992   976773119   104024064    7  HPFS/NTFS/exFAT
/dev/sda5       666327040   768724991    51198976    7  HPFS/NTFS/exFAT
```

```
$ sudo sfdisk -d /dev/sda > sda-backup.txt
Warning: extended partition does not start at a cylinder boundary.
DOS and Linux will interpret the contents differently.
```

Dans ``/etc/default/grub`` :
```
GRUB_HIDDEN_TIMEOUT=10
GRUB_HIDDEN_TIMEOUT_QUIET=false
GRUB_DEFAULT="Windows 8 (loader) (sur /dev/sda1)"
```
puis `update-grub`


Références
----------
- http://gparted.org/h2-fix-msdos-pt.php#overlapping-partitions
- Ubuntu 14.04 en dual-boot avec Windows 8 préinstallé http://forum.ubuntu-fr.org/viewtopic.php?id=1555511
- Boot-repair http://forum.ubuntu-fr.org/viewtopic.php?pid=16690381#p16690381
- Ubuntu UEFI http://doc.ubuntu-fr.org/uefi
