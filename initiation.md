---
title: "Formation Linux : débutant"
author: "Guilhem Bonnefille <guilhem.bonnefille@gmail.com>"
---

Objectif : découvrir le PC à travers Linux.

L'utilisateur n'est pas administrateur de son poste.
Il se limite à l'utiliser.

Cas d'utilisation d'un utilisateur du club.
Cas d'utilisation d'un utilisateur d'une clé.

Intro
===
Ubuntu est la distribution GNU/Linux la plus orientée grand public du moment.

Nous choisissons Ubuntu car, en 2013, c'est encore la distribution GNU/Linux qui est le plus fortement orientée grand public.

Login
===

Unity
===

Manipulation des programmes : démarrage.

Manipulation des fenêtres.

Manipulation fichiers
===
Arborescence
Une seule arborescence : généralement inutile de connaître les détails techniques.

Fichiers, clé, CD-Rom

Nautilus
---
Similitudes et différences entre Linux et Windows.

TP : créer, supprimer, restaurer des dossiers et fichiers.

Fichiers cachés

TP : cacher un fichier, le faire apparaître dans Nautilus.

Jouer avec les différents affichages de Nautilus : liste, détails, barre d'état, panneau latéral...

Logiciels
===

Impression
---
Plug&Play
Avec outils Ubuntu

Direct CUPS
http://localhost:631/

Utilisation des PPD.

Bureautique
---
OpenOffice

Image
---
Gimp

Shotwell

* http://www.omgubuntu.co.uk/2010/08/a-beginners-tour-of-shotwell-ubuntu%E2%80%99s-new-photo-manager

Scanner

* Les principes : plug'n'play
* SimpleScan
* Scan depuis Gimp
* Scan évolué (xscanimage ?)

Hugin

* http://hugin.sourceforge.net/tutorials/two-photos/fr.shtml
* http://www.360x180.fr/blog/category/tutoriel-hugin/

Vignettage

Avec photoprint

Capture d'écran

* http://shutter-project.org/

Messagerie
---

* Thunderbird
* Evolution

Musique
---

* Banshee
* rhythmbox
* Audacity

* Ecouter de la musique.
* Extraire un CD

Vidéo
---

(Voir les vidéos)

Totem : utilise un système à plugins (GStreamer) qui nécessite généralement quelques ajouts avant que ça fonctionne pour les formats "classiques"

Vlc : le logiciel passe-partout : connait beaucoup de format et fonctionne sur toutes les plateformes.

(Édition)

<http://cumulonimbus.fr/index.php?post/2016/01/18/%C3%89diteurs-video-linux>

Webcam
---

Attention au choix de la webcam : beaucoup sont pris en charge, mais certaines ne marcheront pas.

* Cheese
    * Logiciel par défaut
    * Permet de faire des photos, vidéos ou rafale
    * Permet d'appliquer des effets
* VLC
    * Peut accéder à la webcam
    * Par exemple pour diffuser ou enregistrer une petite séquence

Messagerie instantanée
---
* Principes
* Proximité
* Empathy et les multi-protocoles
* IRC
  * #cim sur freenode
  
Vidéo conférence :
* Skype (ne fonctionne plus sous Linux)
* WebRTC, la visio dans le navigateur
  * http://hubl.in/ fournit par Linagora
  * http://meet.jit.si/
  * http://jitsi.tetaneutral.net/

Gravure CD/DVD
---
* Brasero

Sauvegarde
---
Les principes :
* Complète
* Incrémentale
* De la nécessité d'archiver au loin

Les logiciels :
* Déjà-dup
* BackInTime

Le cas CloneZilla.
Cf. Linux Essentiel n°36

Ecran
---
Gestion de plusieurs écran.
Bureau étendu.

Références
===

* http://doc.ubuntu-fr.org/debutant
* http://doc.ubuntu-fr.org/initiation
* http://prof-tux.fr/gratuits.html
* http://gilbert94.jimdo.com/
