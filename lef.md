Préparation
===========

Compte lef
----------
* Création compte lef
* Sélection fond d'écran
* Icone du profil
* Sélection des raccourcis

Mise à jour
-----------
Tous les postes

Jeux
----
* Teeworlds
* [World of PadMan](http://sourceforge.net/projects/worldofpadman/)
* [Minetest](http://minetest.fr/)
    * [Installation Framinetest](https://framacloud.org/fr/cultiver-son-jardin/minetest.html)
* [0.A.D.](http://trac.wildfiregames.com/wiki/Manual_SettingUpAGame)