---
title: "Live system"
---

# Solutions

* Framakey
* Puppy / Toutou
* xubuntu
* http://porteus.org/

# Framakey Ubuntu Remix

<http://framakey.org/>

Basée sur Ubuntu 12.04

# Toutou

Impossible à installer sur clé facilement.

# Porteus

<http://porteus.org/>

Version 3.1 en date du 09 décembre 2014.

# Listes

- https://livecdlist.com/
