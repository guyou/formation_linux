% Formation Linux : perfectionnement
% Guilhem Bonnefille <guilhem.bonnefille@gmail.com>
%

Objectif : autonomie.

L'utilisateur est autonome avec son PC.
Il fait les installations, les mises à jour.

Intro
===
Objectif : devenir autonome pour gérer/administrer son PC
Mises à jour, configuration...

Concepts
===

Le noyau
Les programmes
Le shell

Paquets
===
Concepts
---
Package = partie de logiciel
.deb, .rpm

Notion de dépôt/source
---
Universe...

Le fichier `sources.list`

Ajout d'un dépôt

Installation d'un paquet PPA
---

`sudo add-apt-repository ppa:otto-kesselgulasch/gimp && sudo apt-get update && sudo apt-get install gimp`

Gestionnaires de bureau
===
(_Desktop Environment_)

* Ubuntu / Unity (3D et 2D)
* Gnome / KDE
* XFCE
* LXDE : léger mais encore intuitif (standards actuels)
* Fluxbox
* Console de dépannage

Système
===
Mémoire
---
Observation de la consommation mémoire.

**Graphique :** Moniteur Système

**Ligne de commande :** `free`


A faire :

* Comparer les deux.
* Où sont les informations équivalentes ?

Matériel
---
Pilotes
Pilote vidéo

Divers
===

Stockage cloud
---
Quel usage ?
* Stockage
* Partage
* Synchronisation
Mais il y a aussi une interface web, accessible depuis n'importe où.

Quelle solution ?
* DropBox (2Go)
* Google Drive
* Hubic (25Go)
* Orange / SFR

Notre préférence : HubiC car client Linux, espace gratuit conséquent et hébergé en France.

Chiffrement
* avec gnupg
* avec encfs

Nettoyage du système
===

Au fil du temps, des paquets inutiles peuvent s'accumuler.

Vérifier l'occupation disque :

    $ df -k

Noyau
---
C'est un paquet, donc une mise à jour devrait remplacer la version précédente.
Néanmoins, le noyau est un logiciel particulier, fondamental pour le fonctionnement.
Du coup, il fait l'objet d'un traitement particulier de sorte qu'une mise à jour ne supprime pas (automatiquement) la version précédente.

Ainsi, après mise à jour et redémarrage (il faut redémarrer pour prendre en compte un nouveau noyau), si on constate que la nouvelle version fonctionne mal, on peut toujours redémarrer sur une autre version pour procéder aux investigations et corrections.
Lorsqu'on est certain que la nouvelle version du noyau convient, on peut supprimer les précédentes.

Rechercher :

- `linux-image-X.Y.Z`
- `linux-headers-X.Y.Z`

Mais il ne faut pas couper la branche sur laquelle on est assis.
Il faut donc vérifier la version courante :

    $ uname -a


Paquets inutiles
---
Au fil des mises à jour, installation, suppression de programes, des paquets peuvent être installés mais être devenus inutiles.

Avec synaptic :

- Etat = Locaux ou obsolètes
- Etat = Installés (pouvant être supprimés)

Ces listes ne doivent pas être suivies aveuglément.
Il faut prendre le temps de vérifier qu'elles ne contiennent pas des logiciels dont on ce sert.
Typiquement, la première contient les paquets dits locaux, c'est à dire des paquets que l'on a installés sans passer par les dépots Ubuntu, sans doute en les téléchargeant directement (par exemple : skype).

Janitor
---
Ubuntu propose un logiciel qui permet de faire du ménage :

- programme : `janitor`
- description : Nettoyage du système

Vie privée
===
Sous unity : Paramètres système -> Vie privée
