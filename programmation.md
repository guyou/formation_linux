Enseigner la programmation

Python
======

* http://python-liesse.enseeiht.fr/cours/
* https://openclassrooms.com/courses/apprenez-a-programmer-en-python

Ressources
==========
Articles :

* http://tipes.wordpress.com/2012/07/20/quoi-de-neuf-pour-enseigner-la-programmation/
* http://www.simple-it.fr/blog/2013/08/enseigner-la-programmation-aux-enfants-oui-mais-comment/
* http://mindsized.org/spip.php?article211

Outils :

* http://code.google.com/p/blockly/?redir=1 :  blockly, A visual programming editor 
* http://tournoyons.com/ : Tournois de logiciel
* http://www.irit.fr/ProgAndPlay/index.php : Prog & Play
* http://www.codecademy.com/
* http://scratch.mit.edu

Collection :

* [12 Sites That Will Teach You Coding for Free](https://www.entrepreneur.com/article/250323)