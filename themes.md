% Formation Linux : thèmes
% Guilhem Bonnefille <guilhem.bonnefille@gmail.com>
%

Une série de thèmes autour du *Libre* (avec un grand L).

Open-Data
===

Wikipedia
---

WikiVoyage
---
Guide de voyage du monde entier libre, complet, à jour et fiable.

http://www.wikivoyage.org/

OpenStreetMap
---

### Site officiel

* Rendu utilisable, mais avant tout orienté contributions
* Editeur en ligne
* La bande passante coute cher


### MapQuest

* Résolument orienté utilisateur final
* Rendu plus simple
* Une interface plus conviviale

Logiciel
===
Framasoft
---
Trouver des logiciels libres

Framakey
---
Un système linux dans la poche

Scratch
---

Inkscape
---
Affiche LeF

Blender
---
Avec inkscape

Distribution
===

Alternatives
---

Handy Linux:
Poussée par Combustible.

Framakey
---

Toutou Linux
---
Version portable.
Simple à utiliser.
Sans risque pour les enfants.
Sans risque pour le PC.

Framasoft
===
Les différents services proposés par framasoft :
* l'annuaire
* framadate
* framabag

Hébergement de ces services au club pour les adhérents ?

Divers
===

Licences
---
S'y retrouver un peu

Confidentialité
---
GPG, Tails, Thor

Pb de pirate ?
Secret médical, secret bancaire...

Jouer sous Linux
===

* Steam
