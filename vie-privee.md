---
title: "Vie privée"
---

Vie privée
==========

Rien à cacher ?
---------------

Mots de passe
=============

* Keepass2
* KeepassX

Keepass2 nécessite Mono.
Installation des traductions : `~/.config/KeePass/`.

Protection
==========

Firefox
-------

* Privacy Badger
* uBlock Origin

Anonymat
========

* Tor
* Tails
    * http://tails.boum.org/
* Moteurs de recherche alternatif

Auto-hébergement
================

* OwnCloud

Services moins exposés
======================

* Hubic

Services respectueux
====================

* Framasoft
    * Dégooglisons Internet http://degooglisons-internet.org/
* Associations locales
